(function($) {
	$(function() {
		var puzzleData = [
			{
				clue: " 3. Абсолютная монархия.",
				answer: "автократия",
				position: 3,
				orientation: "across",
				startx: 6,
				starty: 2,
				visible: true,
				color: '#fff'
			},
			{
				clue: " 6. Независимая проверка хозяйственной деятельности.",
				answer: "аудит",
				position: 6,
				orientation: "across",
				startx: 2,
				starty: 3
			},
			{
				clue: " 8. Левая сторона бухгалтерского счета.",
				answer: "дебет",
				position: 8,
				orientation: "across",
				startx: 15,
				starty: 3
			},
			{
				clue: "11. Древняя счетная доска.",
				answer: "абак",
				position: 11,
				orientation: "across",
				startx: 1,
				starty: 5
			},
			{
				clue: "12. Документ с детальным изложением обсуждаемых вопросов.",
				answer: "меморандум",
				position: 12,
				orientation: "across",
				startx: 6,
				starty: 5
			},
			{
				clue: "13. В бухгалтерии: ведение дел, при котором каждая операция регистрируется немедленно после ее совершения.",
				answer: "ажур",
				position: 13,
				orientation: "across",
				startx: 17,
				starty: 5
			},
			{
				clue: "14. Оплата определенного процента при финансовых операциях.",
				answer: "ажио",
				position: 14,
				orientation: "across",
				startx: 6,
				starty: 7
			},
			{
				clue: "16. «Затылок» у топора, погубивший старуху-процентщицу.",
				answer: "обух",
				position: 16,
				orientation: "across",
				startx: 12,
				starty: 7
			},
			{
				clue: "19. Она, согласно пословице, и выделки не стоит.",
				answer: "овчина",
				position: 19,
				orientation: "across",
				startx: 8,
				starty: 8
			},
			{
				clue: "20. Публичный показ.",
				answer: "смотр",
				position: 20,
				orientation: "across",
				startx: 4,
				starty: 9
			},
			{
				clue: "21. Потеря в весе или объеме товара, происходящая вследствие его вытекания или просыпки из тары либо из транспортных средств.",
				answer: "лекаж",
				position: 21,
				orientation: "across",
				startx: 13,
				starty: 9
			},
			{
				clue: "23. В мультике про «Короля льва» этот зверь всегда рядом со львами разгуливал.",
				answer: "гиена",
				position: 23,
				orientation: "across",
				startx: 4,
				starty: 11
			},
			{
				clue: "24. Автор «Приключений Незнайки».",
				answer: "носов",
				position: 24,
				orientation: "across",
				startx: 13,
				starty: 11
			},
			{
				clue: "25. Чак, «Крутой Уоккер».",
				answer: "норрис",
				position: 25,
				orientation: "across",
				startx: 8,
				starty: 12
			},
			{
				clue: "28. Наш знаменитый самолет.",
				answer: "ту",
				position: 28,
				orientation: "across",
				startx: 6,
				starty: 13
			},
			{
				clue: "30. Это редкое женское имя означает «фиалка».",
				answer: "ия",
				position: 30,
				orientation: "across",
				startx: 14,
				starty: 13
			},
			{
				clue: "31. Город в США, пригород Лос-Анджелеса.",
				answer: "глендейл",
				position: 31,
				orientation: "across",
				startx: 7,
				starty: 14
			},
			{
				clue: "32. Диснеевский Пумба по своей «национальности».",
				answer: "бородавочник",
				position: 32,
				orientation: "across",
				startx: 5,
				starty: 17
			},
			{
				clue: "34. Торговая марка.",
				answer: "бренд",
				position: 34,
				orientation: "across",
				startx: 3,
				starty: 19
			},
			{
				clue: "35. Товар на бирже. ",
				answer: "акция",
				position: 35,
				orientation: "across",
				startx: 14,
				starty: 19
			},

			{
				clue: " 1. «Место встречи» в Интернете.",
				answer: "чат",
				position: 1,
				orientation: "down",
				startx: 6,
				starty: 1
			},
			{
				clue: " 2. И «Охотный», и просто торговый.",
				answer: "ряд",
				position: 2,
				orientation: "down",
				startx: 15,
				starty: 1
			},
			{
				clue: " 4. Роман Гончарова о ленивом мечтателе.",
				answer: "обломов",
				position: 4,
				orientation: "down",
				startx: 9,
				starty: 2
			},
			{
				clue: " 5. Отказ грузовладельца от своих прав на застрахованное имущество при уплате ему страховой суммы.",
				answer: "абандон",
				position: 5,
				orientation: "down",
				startx: 12,
				starty: 2
			},
			{
				clue: " 6. Пророк Иов у мусульман.",
				answer: "аюб",
				position: 6,
				orientation: "down",
				startx: 2,
				starty: 3
			},
			{
				clue: " 7. Имя футбольного тренера по фамилии Адвокат.",
				answer: "дик",
				position: 7,
				orientation: "down",
				startx: 4,
				starty: 3
			},
			{
				clue: " 9. Вид светильника.",
				answer: "бра",
				position: 9,
				orientation: "down",
				startx: 17,
				starty: 3
			},
			{
				clue: "10. Буква греческого алфавита.",
				answer: "тау",
				position: 10,
				orientation: "down",
				startx: 19,
				starty: 3
			},
			{
				clue: "14. Лицо, имеющее право пользоваться чем-либо.",
				answer: "абонент",
				position: 14,
				orientation: "down",
				startx: 6,
				starty: 7
			},
			{
				clue: "15. Самая известная «христианская» река.",
				answer: "иордан",
				position: 15,
				orientation: "down",
				startx: 8,
				starty: 7
			},
			{
				clue: "17. Сравнительный итог прихода и расхода.",
				answer: "баланс",
				position: 17,
				orientation: "down",
				startx: 13,
				starty: 7
			},
			{
				clue: "18. Республика России со столицей в Абакане.",
				answer: "хакасия",
				position: 18,
				orientation: "down",
				startx: 15,
				starty: 7
			},
			{
				clue: "20. Ценная промысловая рыба.",
				answer: "сиг",
				position: 20,
				orientation: "down",
				startx: 4,
				starty: 9
			},
			{
				clue: "22. Французский писатель – «Трагические поэмы», «Пустынный мир».",
				answer: "жув",
				position: 22,
				orientation: "down",
				startx: 17,
				starty: 9
			},
			{
				clue: "26. Пулеметная или льготная.",
				answer: "очередь",
				position: 26,
				orientation: "down",
				startx: 9,
				starty: 12
			},
			{
				clue: "27. Вдохновитель партии.",
				answer: "идеолог",
				position: 27,
				orientation: "down",
				startx: 12,
				starty: 12
			},
			{
				clue: "29. В таблице у Менделеева идет после бора.",
				answer: "углерод",
				position: 29,
				orientation: "down",
				startx: 7,
				starty: 13
			},
			{
				clue: "30. Старинная улица в Москве.",
				answer: "ильинка",
				position: 30,
				orientation: "down",
				startx: 14,
				starty: 13
			},
			{
				clue: "32. Плоскогорье в центральной части Анголы.",
				answer: "бие",
				position: 32,
				orientation: "down",
				startx: 5,
				starty: 17
			},
			{
				clue: "33. Настоящая фамилия Романа Карцева.",
				answer: "кац",
				position: 33,
				orientation: "down",
				startx: 16,
				starty: 17
			},
			{
				clue: " ",
				answer: " ",
				position: 41,
				orientation: "down",
				startx: 0,
				starty: 0
			}


		]

		$('#puzzle-wrapper').crossword(puzzleData);

	})

})(jQuery)
