$(function(){

    function clear(c, a, b){
        while(c.indexOf(a) > 0)c = c.replace(a, b);
        return c;
    }

    $header.add($text).remove();

    $area.append('<div class="slider__wrapper">'+
        '<div class="slider__title"><span class="slider__title-text"></span></div>' +
        '<div class="slider__window"></div>' +
        '<div class="slider__thumbs">' +
        '<div class="slider__thumbs-prev"></div>' +
        '<div class="slider__thumbs-next"></div>' +
        '<div class="slider__thumbs-window">' +
        '<div class="slider__thumbs-container">' +
        '</div></div></div></div>');

    var ratio = 1.558,
        $wrap = $('.slider__wrapper'),
        $win = $('.slider__window'),
        $thumbs,
        $images,
        $tWrap = $('.slider__thumbs-container'),
        $tLeft = $('.slider__thumbs-prev'),
        $tRight = $('.slider__thumbs-next'),
        slidesLength,
        currentSlide = 0,
        thumbsPos = 0,
        thumbSpace = 0,
        images = [],
        texts = [],
        isResizing = false;

    $check.hide();
    $retry.hide();
    $('#stageHead').hide();

    loadXml(function(){
        parseData();

        var $images = $('.slider__window-item img'),
            imgLength = $images.length,
            imgCounter = 0;

        $images.each(function(){

            $(this).load(function(){
                imgCounter++;

                if(imgCounter == imgLength){
                    resizeSlider();
                    $(window).trigger('allpreloaded');
                }
            });

        });

        taskResizeHolder = resizeSlider;

    });

    function parseData(){
        xml = clear(xml, 'image', 'span');

        var $sandbox = $('<div/>').append(xml);

        $sandbox.find('span').each(function(){
            images.push($(this).attr('src'));
            texts.push($(this).text());
        });

        images.forEach(function(item, i){
            var $thumb = $('<div/>', {class: 'slider__thumbs-item'});
            $thumb.append($('<img/>', {src: config.contentFolder + item}));
            $tWrap.append($thumb);

            var $item = $('<div/>', {class: 'slider__window-item'});
            $item.append($('<img/>', {src: config.contentFolder + item})).append($('<div/>', {class: 'slider__item-text', text: texts[i]}));
            $win.append($item);

            var $img = $item.find('img');

            $img.load(function(){
                if($img.width()/$img.height() > 1.86){$img.addClass('horizontal')}
                else {$img.addClass('vertical')}
                //console.log($img);
                //console.log($img.width()/$img.height());

            });
        });

        $thumbs = $('.slider__thumbs-item');
        $images = $('.slider__window-item');
        slidesLength = $images.length;

        $thumbs.click(function(){
            currentSlide = $(this).index();
            $thumbs.removeClass('active').eq(currentSlide).addClass('active');
            $images.removeClass('active').eq(currentSlide).addClass('active');
        });

        $tLeft.on(clickEvent, function(){move(-1)});
        $tRight.on(clickEvent, function(){move(1)});
        $images.on(clickEvent, function(){showFullscreenImage($(this).find('img').attr('src'), $(this).find('.slider__item-text').html())});


        $images.eq(0).addClass('active');
        $thumbs.eq(0).addClass('active');

        // title
        if(config.title) $('.slider__title-text').html(config.title);

    }

    function resizeSlider(){

        var winVerticalSpace = window.innerHeight - $win.offset().top - $('.cp-button-container').outerHeight(),
            winRatio = window.innerWidth / winVerticalSpace;

        if(winRatio > ratio){
            $wrap.css({height: winVerticalSpace, width: winVerticalSpace*ratio});
        } else {
            $wrap.css({width: window.innerWidth, height: window.innerWidth/ratio});
        }


        isResizing = true;
        //if(window.innerWidth >= window.innerHeight*ratio){
        //    $wrap.css({height: window.innerHeight - 220 + 'px', margin: '70px auto 0'}).css({'width': $wrap.height()*ratio + 'px'});
        //} else {
        //    $wrap.css({width: '90%', margin: '70px 5% 0'}).css({'height': $wrap.width()/ratio + 'px'});
        //}

        $thumbs.css('width', $thumbs.height() + 'px');
        $thumbs.css('margin', '0 ' + $thumbs.height()/5.5 + 'px');
        thumbSpace = $thumbs.width()+($thumbs.width()/5.5)*2;

        $images.each(function(){
            var $el = $(this),
                $i = $el.find('img'),
                $t = $el.find('.slider__item-text');
            $t.css({width: $i.width() + 'px', marginLeft: -$i.width()/2 + 'px'});
        });
        setTimeout(function(){isResizing = false}, 500);
    }


    function move(dir){
        currentSlide+=dir;

        if(currentSlide < 0) currentSlide = 0;
        else if (currentSlide > slidesLength - 1) currentSlide = slidesLength - 1;

        if($thumbs.length > 4){
            thumbsPos=currentSlide;
            if(thumbsPos > slidesLength - 4){thumbsPos=slidesLength - 4;}
            $tWrap.css('left', -thumbsPos*thumbSpace + 'px');
        }

        $images.removeClass('active').eq(currentSlide).addClass('active');
        $thumbs.removeClass('active').eq(currentSlide).addClass('active');
    }

});